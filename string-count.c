#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <inttypes.h>
#include <fcntl.h>
#include <string.h>

/*
 * This code is intentionally broken in multiple aspects.
 * Find and fix errors ! :)
 */

#define MAX_THREADS 0x40
#define MAX_STRING 124ULL
#define MAX_CHUNKS 40
#define READ_CHUNK_SIZE MAX_STRING
#define READ_THREAD_WAIT_CHUNKS_REMOVAL 10
#define WORKER_THREAD_WAIT_CHUNK_ADDITION 2

pthread_mutex_t cct_mutex;
pthread_mutex_t sc_mutex;
int read_finished = 0;


struct string_count_entry
{
	int count;
	char *string;
};

struct string_count
{
	int nb_string;
	struct string_count_entry *entries;
};

struct worker_thread_arg
{
	struct chunk_container *cct;
	struct string_count *sc;
};

struct chunk_container
{
	int count;
	char **chunks;
};

struct read_thread_arg
{
	int fd;
	struct chunk_container *cct;
};

struct chunk_container *chunk_container_init()
{
	struct chunk_container *cct = malloc(sizeof *cct);
	cct->count = 0;
	cct->chunks = malloc(MAX_CHUNKS * sizeof *cct->chunks);
	return cct;
}

void chunk_container_free(void *t)
{
	struct chunk_container *cct = t;
	for (int i = 0; i < cct->count; ++i)
	{
		free(cct->chunks[i]);
	}
	free(cct->chunks);
	free(cct);
	cct = NULL;
}

struct string_count *string_count_init(void)
{
	struct string_count *sc = malloc(sizeof *sc);
	sc->nb_string = 0;
	sc->entries = NULL;
	return sc;
}

void string_count_free(void *pt)
{
	struct string_count *sc = pt;
	for (int i = 0; i < sc->nb_string; i++)
	{
		free(sc->entries[i].string);
	}
	free(sc->entries);
	free(sc);
	sc = NULL;
}

struct read_thread_arg *read_thread_arg_init(int fd, struct chunk_container *cct)
{
	struct read_thread_arg *rt_arg = malloc(sizeof *rt_arg);
	rt_arg->fd = fd;
	rt_arg->cct = cct;
	return rt_arg;
}

void read_thread_arg_free(void *t)
{
	free(t);
	t = NULL;
}

struct worker_thread_arg *work_thread_arg_init(struct chunk_container *cct)
{
	struct worker_thread_arg *wt_arg = malloc(sizeof *wt_arg);
	wt_arg->cct = cct;
	wt_arg->sc = string_count_init();
	return wt_arg;
}

void worker_thread_arg_free(void *t)
{
	struct worker_thread_arg *wt_arg = (struct worker_thread_arg*)t;
	string_count_free(wt_arg->sc);
	wt_arg->cct = NULL;
	free(wt_arg);
	wt_arg = NULL;
}

int addstring(struct string_count *pt, char *s)
{
	int i;
	int nb_string = __atomic_load_n(&pt->nb_string, __ATOMIC_SEQ_CST);
	for (i = 0; i < nb_string; i++)
	{
		// FIXED break condition when the string is found
		if (strcmp(__atomic_load_n(&pt->entries[i].string, __ATOMIC_SEQ_CST), s) == 0)
		{
			break;
		}
	}

	struct string_count_entry *entries = __atomic_load_n(&pt->entries, __ATOMIC_SEQ_CST);
	if (i == nb_string)
	{
		struct string_count_entry *new_entries = realloc(entries, (nb_string + 1) * sizeof *new_entries);

		if (new_entries == NULL)
			return -1;

		__atomic_store_n(&pt->entries, new_entries, __ATOMIC_SEQ_CST);
		__atomic_fetch_add(&pt->nb_string, 1, __ATOMIC_SEQ_CST);

		char *entry_string = malloc((strlen(s) + 1) * sizeof *entry_string);
		strcpy(entry_string, s);
		__atomic_store_n(&pt->entries[i].string, entry_string, __ATOMIC_SEQ_CST);
		__atomic_store_n(&pt->entries[i].count, 0, __ATOMIC_SEQ_CST);
	}

	__atomic_fetch_add(&pt->entries[i].count, 1, __ATOMIC_SEQ_CST);
	return 0;
}

char *readline(int fd)
{
	int i = 0;
	char c;
	// FIXED The linebuff memory allocation by using malloc.
	//       The storage for a local variable is lost as soon as the function returns
	//       due to the way the stack works, but malloc allocates memory on the heap.
	char *linebuf = malloc(sizeof (char) * MAX_STRING);

	// FIXED the read call by passing an actual file descriptor instead of 0 which defaults to console input.
	while (read(fd, &c, 1) != 0) {
		if (c == '\n') {
			linebuf[i] = '\0';
			return linebuf;
		}

		// FIXED line buffer overflow by trimming down the line
		if (i < MAX_STRING -1)
		{
			linebuf[i++] = c;
		}
	}

	return NULL;
}

char *get_long_line_leftover(int fd, char *chunk)
{
	char *long_line_leftover = NULL;

	// If there is a super long line
	if (strrchr(chunk, '\n') == NULL)
	{
		char *tmp = malloc((READ_CHUNK_SIZE + 1) * sizeof *tmp);
		char *found_newline = NULL;
		int read_count = 0;
		do
		{
			// Continue reading until the first chunk with a newline is reached
			read_count = read(fd, tmp, READ_CHUNK_SIZE) / sizeof *tmp;
			if(read_count > 0)
			{
				tmp[read_count] = '\0';
				found_newline = strrchr(tmp, '\n');
			}
		}
		while (read_count != 0 && !found_newline);

		char *end_leftover = NULL;

		// If after the newline there are more characters
		if(found_newline)
		{
			if(strlen(found_newline) > 1)
			{
				// Strip the newline character by moving to the next char
				end_leftover = found_newline + 1;
			}
		}

		// And remember the leftover that is at the end
		if(end_leftover)
		{
			int long_line_size = (strlen(end_leftover) + 1);
			long_line_leftover = malloc(long_line_size * sizeof *long_line_leftover);
			strcpy(long_line_leftover, end_leftover);
		}
		free(tmp);
	}
	return long_line_leftover;
}

// Read chunks but trim each chunk to the last new line and set a leftover
char *readchunk(int fd, char** leftover)
{
	char *chunk = malloc((READ_CHUNK_SIZE + 1) * sizeof *chunk);
	int chunk_size = read(fd, chunk, READ_CHUNK_SIZE) / sizeof *chunk;

	if (chunk_size > 0)
	{
		chunk[chunk_size] = '\0';

		// If the chunk is part of a huge line continue reading chunks
		// until one has newlines and take the leftover after the newline
		char *long_line_leftover = get_long_line_leftover(fd, chunk);

		// Now let's handle the chunk
		char *final_chunk = chunk;
		int final_chunk_size = chunk_size;

		// If there's previous leftover
		if(*leftover != NULL)
		{
			int leftover_size = strlen(*leftover);
			final_chunk_size += leftover_size;
			final_chunk = malloc((final_chunk_size + 1) * sizeof *final_chunk);
			strcpy(final_chunk, *leftover);
			strcat(final_chunk, chunk);
			free(chunk);
		}

		char *new_leftover = strrchr(final_chunk, '\n');

		if (new_leftover)
		{
			if (strlen(new_leftover) > 1)
			{
				new_leftover = new_leftover + 1;
			}
			else
			{
				new_leftover = NULL;
			}
		}

		if (long_line_leftover)
		{
			*leftover = realloc(*leftover, (strlen(long_line_leftover) + 1) * sizeof **leftover);
			strcpy(*leftover, long_line_leftover);
			free(long_line_leftover);
		}
		else
		{
			if (new_leftover)
			{
				*leftover = realloc(*leftover, (strlen(new_leftover) + 1) * sizeof **leftover);
				strcpy(*leftover, new_leftover);
			}
			else
			{
				free(*leftover);
				*leftover = NULL;
			}
		}

		if (new_leftover != NULL)
		{
			final_chunk_size = final_chunk_size - strlen(new_leftover);
		}

		// If the final chunk needs resizing
		if (final_chunk_size != chunk_size)
		{
			final_chunk_size = (final_chunk_size < READ_CHUNK_SIZE ? final_chunk_size : READ_CHUNK_SIZE);
			final_chunk = realloc(final_chunk, (final_chunk_size + 1) * sizeof *final_chunk);
			final_chunk[final_chunk_size] = '\0';
		}

		return final_chunk;
	}

	return NULL;
}

int addchunk(struct chunk_container *cct, char* chunk)
{
	char **new_chunks = realloc(__atomic_load_n(&cct->chunks, __ATOMIC_SEQ_CST),
			(__atomic_load_n(&cct->count, __ATOMIC_SEQ_CST) + 1) * sizeof *new_chunks);
	__atomic_store_n(&cct->chunks, new_chunks, __ATOMIC_SEQ_CST);

	int index = __atomic_load_n(&cct->count, __ATOMIC_SEQ_CST);
	__atomic_store_n(&cct->chunks[index], chunk, __ATOMIC_SEQ_CST);
	__atomic_fetch_add(&cct->count, 1, __ATOMIC_SEQ_CST);
	return 0;
}

char *take_chunk(struct chunk_container *cct)
{
	if (__atomic_load_n(&cct->count, __ATOMIC_SEQ_CST) > 0)
	{
		// TODO: Check if it takes the 0 index element
		int index = __atomic_load_n(&cct->count, __ATOMIC_SEQ_CST);
		char *cct_chunk = __atomic_load_n(&cct->chunks[index - 1], __ATOMIC_SEQ_CST);
		char *chunk = malloc((strlen(cct_chunk) + 1) * sizeof *chunk);
		strcpy(chunk, cct_chunk);
		char **new_chunks = realloc(__atomic_load_n(&cct->chunks, __ATOMIC_SEQ_CST),
					(__atomic_load_n(&cct->count, __ATOMIC_SEQ_CST) - 1) * sizeof *new_chunks);
		__atomic_store_n(&cct->chunks, new_chunks, __ATOMIC_SEQ_CST);
		__atomic_fetch_sub(&cct->count, 1, __ATOMIC_SEQ_CST);
		return chunk;
	}
	return NULL;
}

void *read_thread_main(void *arg)
{
	struct read_thread_arg *rt_arg = arg;
	char *chunk = NULL;
	char *leftover = NULL;
	int read_last = 0;

	do
	{
		chunk = readchunk(rt_arg->fd, &leftover);

		// When reaching the end and there's still a leftover, set the it as the chunk
		if(chunk == NULL)
		{
			if (leftover != NULL)
			{
				chunk = leftover;
			}
			read_last = 1;
		}

		if(chunk != NULL)
		{
			// Wait if the chunks container is full
			while(__atomic_load_n(&rt_arg->cct->count, __ATOMIC_SEQ_CST) == MAX_CHUNKS)
			{
				usleep(READ_THREAD_WAIT_CHUNKS_REMOVAL);
			}

			pthread_mutex_lock(&cct_mutex);

			addchunk(__atomic_load_n(&rt_arg->cct, __ATOMIC_SEQ_CST), chunk);

			pthread_mutex_unlock(&cct_mutex);
		}
	}
	while (chunk != NULL && !read_last);

	__atomic_store_n(&read_finished, 1, __ATOMIC_SEQ_CST);

	return NULL;
}

void *worker_thread_main(void *arg)
{
	struct worker_thread_arg *wt_arg = arg;
	char *chunk = NULL;
	int all_done = 0;

	do
	{
		pthread_mutex_lock(&cct_mutex);

		chunk = take_chunk(__atomic_load_n(&wt_arg->cct, __ATOMIC_SEQ_CST));

		pthread_mutex_unlock(&cct_mutex);

		if(chunk != NULL)
		{
			char *line = strtok(chunk, "\n");

			while (line != NULL)
			{
				// Lock the string count container
				pthread_mutex_lock(&sc_mutex);
				addstring(__atomic_load_n(&wt_arg->sc, __ATOMIC_SEQ_CST), line);
				pthread_mutex_unlock(&sc_mutex);
				line = strtok(NULL, "\n");
			}

			free(chunk);
			chunk = NULL;
		}
		else
		{
			usleep(WORKER_THREAD_WAIT_CHUNK_ADDITION);
		}
		all_done = (chunk == NULL) && (__atomic_load_n(&read_finished, __ATOMIC_SEQ_CST)) && (__atomic_load_n(&wt_arg->cct->count, __ATOMIC_SEQ_CST) == 0);
	} while (!all_done);

	return NULL;
}

// FIXED Compare function by removing inlining. Required for the qsort call.
int Compare(const void *pt1, const void *pt2)
{
	// [WARNING_FIX] Fixed the initialization warning about discarding the const qualifier
	const struct string_count_entry *a = pt2;
	const struct string_count_entry *b = pt1;

	if (a->count == b->count)
		return strcmp(a->string, b->string);
	// [WARNING FIX] Fixed the misleading indentation
	return (a->count - b->count);
}

void string_count_pint(struct string_count *sc)
{
	int i;

	qsort(sc->entries, sc->nb_string, sizeof(struct string_count_entry),
		Compare);

	i = 0;
	while (i < sc->nb_string) {
		printf("%d %s\n", sc->entries[i].count, sc->entries[i].string);
		i++;
	}
}

int main(int argc, char **argv)
{
	int nbthreads;
	int i;
	pthread_t read_thread;
	pthread_t worker_threads[MAX_THREADS];
	struct chunk_container *cct;
	struct read_thread_arg *rt_arg;
	struct worker_thread_arg *wt_arg;

	// FIXED the expected arguments count
	if (argc != 3) {
		// FIXED the usage message
		fprintf(stderr, "usage: <nb threads> %s \n", argv[0]);
		return EXIT_FAILURE;
	}

	// Parse the threads number argument using the more safer strtoimax
	char *temp;
	nbthreads = strtoimax(argv[1], &temp, 0);
	if (temp == argv[1])
	{
		fprintf(stderr, "Invalid input: threads number!");
		return EXIT_FAILURE;
	}
	if (nbthreads < 0)
	{
		fprintf(stderr, "Invalid input: the threads number is negative!");
		return EXIT_FAILURE;
	}
	if (nbthreads > MAX_THREADS)
	{
		fprintf(stdout, "Warning: the threads number [%d] is bigger than the maximum [%d]! "
				"Using the maximum instead!\n", nbthreads, MAX_THREADS);
		nbthreads = MAX_THREADS;
	}

	int fd = open(argv[2], O_RDONLY);

	if (fd < 0)
	{
		fprintf(stderr, "Error: unable to open the file located at %s", argv[2]);
		return EXIT_FAILURE;
	}

	pthread_mutex_init(&cct_mutex, NULL);
	pthread_mutex_init(&sc_mutex, NULL);

	cct = chunk_container_init();
	rt_arg = read_thread_arg_init(fd, cct);
	wt_arg = work_thread_arg_init(cct);

	pthread_create(&read_thread, NULL, read_thread_main, rt_arg);

	for (i = 0; i < nbthreads; i++) {
		pthread_create(&worker_threads[i], NULL, worker_thread_main, wt_arg);
	}

	for (i = 0; i < nbthreads; i++){
		pthread_join(worker_threads[i], NULL);
	}

	string_count_pint(wt_arg->sc);

	worker_thread_arg_free(wt_arg);
	read_thread_arg_free(rt_arg);
	chunk_container_free(cct);

	pthread_mutex_destroy(&cct_mutex);
	pthread_mutex_destroy(&sc_mutex);

	close(fd);

	return EXIT_SUCCESS;
}

