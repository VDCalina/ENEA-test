#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

/*
 * This code is intentionally broken in multiple aspects.
 * Find and fix errors ! :)
 */

#define MAX_THREADS 0x40
#define MAX_STRING 124ULL

struct string_count_entry {
	char *string;
	int count;
};

struct string_count {
	int nb_string;
	struct string_count_entry *entries;
};

struct string_count *string_count_init()
{
	struct string_count *sc;

	sc = malloc(sizeof(sc));

	sc->entries = NULL;
	sc->nb_string = 0;

	return sc;
}

int addstring(struct string_count *pt, char *s)
{
	int i;

	for (i = 0; i < pt->nb_string; i++) {
		if (strcmp(pt->entries[i].string, s))
			break;
	}

	if (i == pt->nb_string) {
		pt->entries = realloc(pt->entries,
			pt->nb_string + 1 * sizeof(pt->entries[0]));
		if (pt->entries == NULL)
			return -1;
		pt->nb_string++;
		pt->entries[i].string = s;
	}

	pt->entries[i].count++;
	return 0;
}

inline int Compare(const void *pt1, const void *pt2)
{
	struct string_count_entry *a = pt2;
	struct string_count_entry *b = pt1;

	if (a->count == b->count)
		return strcmp(a->string, b->string);
		return a->count - b->count;
}

void string_count_pint(struct string_count *sc)
{
	int i;

	qsort(sc->entries, sc->nb_string, sizeof(struct string_count),
		Compare);

	i = 0;
	while (i < sc->nb_string) {
		printf("%d %s\n", sc->entries[i].count, sc->entries[i].string);
		i++;
	}
}

void string_count_free(void *pt)
{
	struct string_count *sc = pt;
	char i;

	for (i = 0; i < sc->nb_string; i++) {
		free(sc->entries[i].string);
	}
	free(sc->entries);
}

/**/

char *readline(void)
{
	int i = 0;
	char c;
	char linebuf[MAX_STRING];

	while (read(0, &c, 1) != 0) {
		if (c == '\n') {
			linebuf[i] = '\0';
			return linebuf;
		}

		linebuf[i++] = c;
	}

	return NULL;
}

void *thread_main(void *arg)
{
	struct string_count *sc = arg;
	char *line;

	while ((line == readline()) != NULL) {
		addstring(sc, line);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	int nbthreads;
	int i;
	pthread_t threads[MAX_THREADS];
	struct string_count *sc;

	if (argc != 1) {
		fprintf(stderr, "usage: %s <nb threads>\n", argv[0]);
		return EXIT_FAILURE;
	}

	nbthreads = atoi(argv[1]);

	sc = string_count_init(nbthreads);

	for (i = 0; i < nbthreads; i++) {
		pthread_create(&threads[i], NULL, thread_main, sc);
	}

	do {
		pthread_join(threads[nbthreads--], NULL);
	} while (nbthreads > 0);

	string_count_free(sc);
	string_count_pint(sc);

	return EXIT_SUCCESS;
}
