This is an ENEA String Count application
========================================

The application parses an input file and outputs the number of occurences for each line.

Usage
-----
> string-count <nb_threads> <input_file>

Where:
* **nb_threads** represents the number of used threads.
* **input_file** represents the file path.

Limitations
-----------
* The number of maximum allowed threads is 64. If a bigger number is provided, then the application will fallback on using the the maximum.
* The maximum line size is 124 characters. If there are lines bigger than 124 characters, then the application will trim them down.

Possible improvements
---------------------

### Producer - consumer threads (DONE)
Due to the physical limitations of an HDD, the read operation can't be efficiently performed with multiple threads. Depending on how the file is stored on the HDD, the read header might jump on different memory locations, which is quite slow.
Because of that, a better solution would be to have a reader thread that reads chunks from the file and stores them in a container. Those chunks would then be used by the worker threads to do the processing in parallel.
The following is a sequence diagram for the possible improvment:

<!-- Using img tag to modify the img size instead of the normal image adding method -->
<img src="img/Reader-and-worker-threads-diagram.png"  width="400" height="400">

### Parallel read on SSD storage
As mentioned above, due to the HDD limitations, the read operation can't be performed in parallel, but it's different on an SSD.
Due to the fact that SSDs don't have a read head, the read operations can be efficiently performed in parallel.
The most difficult part in doing this is on how to detect if the storage is an SSD or an HDD.
There is a method to do that, more exactly, checking the value from `/sys/block/sda/queue/rotational` (1 for HDD, 0 for SSD) on Linux systems, but this not a general solution nor cross-platform.

### Known bugs
* Consecutive long lines are not parsed well by the read thread
