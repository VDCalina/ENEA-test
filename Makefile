CC=gcc
CFLAGS=-Wall
LDFLAGS=-lpthread
ODIR=obj
BIN=string-count

_OBJ = $(BIN).o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c
	mkdir -p $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BIN): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o
	rmdir $(ODIR)
	rm $(BIN)

